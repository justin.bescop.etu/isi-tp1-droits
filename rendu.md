# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: BESCOP Justin - justin.bescop.etu@univ-lille.fr

- Nom, Prénom, email: FERDINAND Mathieu - mathieu.ferdinand.etu@univ-lille.fr

## Question 1

Non le processus ne peut pas écrire dans titi.txt. En effet comme toto est son propriétaire
et qu'il exécute le processus c'est le 1er triplets de permissions qui sera considéré. 
Dans le cas de titi.txt le 1er triplet accorde un droit en lecture (-read) mais pas en écriture (-write)
Le processus ne peut pas écrire dans titi.txt

## Question 2

Le caractère x dans les triplets de permissions pour un repertoire indique si on a le droit ou non
de le parcourir.

Lorsqu'on essaye de rentrer dans mydir avec toto on ne peut pas "Permission denied"
Lorsqu'on essaye de lister le contenu du repertoire mydir contenant le fichier data.txt avec toto,
on echoue: On obtient des messages d'erreurs pour l'acces à tous les fichiers et repertoire de mydir ("Permission denied")
De plus la sortie de ls nous donne les détails mais on obtient seulement les noms des élements tout le reste
est caché (avec des pts d'interrogation). C'est normal car toto n'a pas le droit de parcourir ce fichier.

## Question 3
1-
En ID on a -> uid:1001 , euid:1001 , gid : 1000 , egid : 1000
Le processus n'arrive pas à ouvrir le fichier , on obtient l'erreur : "Cannot open file : Permission denied"
2- Après activation du flag set-user-id, on obtient les IDs suivants : 
-> uid:1001 , euid:100 , gid : 1000 , egid : 1000
De plus maintenant toto arrive à ouvrir le fichier via l'executable. En effet l'effective ID indique que toto est
considéré comme ubuntu en executant le programme. Ainsi toto se fera passer pour ubuntu pour l'ouverture du fichier aussi


## Question 4

On obtient les IDs suivant ->
EUID : 1001 et EGID : 1000

## Question 5

La commande chfn sert à changer les infos d'un utilisateur. Les groupes et others ont le droit en lecture et execution
et le flag set-user-id est activé. 
Comme en toute logique chaque utilisateur a le droit de changer ses infos il faut permettre au fichier /etc/passwd d'être modifié dans
un cadre précis (ici l'utilisation de la commande chfn).

## Question 6

Les mots de passes sont dans /etc/shadow car cela permet d'avoir une sécurité grace au fait que les mots de passes
soient cryptés ainsi que grace au fait que /etc/shadow ne soit accessible que par root contrairement à /etc/passwd .  De plus le fichier stocke aussi d'autres infos sur les mots de passe qui ne peuvent pas être
contenus dans l'unique champ consacré au mot de passe dans /etc/passwd (représenté par le x).

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.
Note : Le programme n'arrive pas a matcher les mdp des utilisateurs

## Question 9

Le programme et les scripts dans le repertoire *question9*.
Note : le programme n'arrive pas à executer crypt()

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








