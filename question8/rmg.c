#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>

#define BUFFER_SIZE 100
char motDePasse[BUFFER_SIZE];



int main(int argc, char *argv[]){
        //Verifie si on a bien recu un fichier en argument
        if(argc < 2 ) {
                printf("Missing argument\n");
                exit(EXIT_FAILURE);
        }
        //Verifie si on a le droit de lire dans le fichier
        int fd = access(argv[1], R_OK);
        if(fd==-1) {
                printf("L'utilisateur ne fait pas partie du bon groupe\n");
                exit(EXIT_FAILURE);
        }
        printf("L'utilisateur fait partie du bon groupe\n");
        //Demande le mot de passe de l'utilisateur avant de supprimer le fichier
        printf("donnez votre mot de passe :");

        printf("DEBUG\n");
        fgets(motDePasse,BUFFER_SIZE,stdin);

        ///////Verification du mot de passe ////////////
        FILE *f;
        char ligne[BUFFER_SIZE];

        //Recupere le ruid pour trouver la ligne du mdp
        uid_t ident = getuid();
        printf(" uid : %u\t", ident);
        char ruid[BUFFER_SIZE];
        sprintf(ruid, "%d", ident);
        printf("%s\n",ruid);
        printf("DEBUG\n");
        f = fopen("/home/admin/passwd", "r");
        while((fgets(ligne,80,f)) != NULL){
        int user=1;
        int mdp=1;
                //Compare pour chaque ligne l'id (4 premiers car)
                for(int i=0;i<4;i++){
                        if(ruid[i]!=ligne[i]){
                                user=0;
                                //printf("DEBUG %s\n",ligne);
                        }
                }
                if(user){
                        printf("%s\n",&ligne[4]);
                        for(int i=5;i<25;i++){//le ruid comporte 4 chiffres, il est séparé du mdp dans le fichier par":" donc le mot de passe commence au 6 eme caractere
                        if(motDePasse[i]!=ligne[i]){
                                        mdp=0;
                                }
                        }
                        if(mdp){
                                remove(argv[1]);
                        }
                        else{ printf("le mot de passe n'est pas bon");
                        }
                }
                else{
                printf("Utilisateur sans mdp ");
                }
        }
        printf("DEBUG fin\n");
        fclose(f);
        return 0;
}