#!/bin/sh

if [ -w ../dir_a/ ];then
    echo " l'utilisateur a acces au dossier dir_a en ecriture"
else
    echo " l'utilisateur n'a pas acces au dossier dir_a en ecriture"
fi
if [ -w ../dir_b/ ];then
    echo " l'utilisateur a acces au dossier dir_b en ecriture"
else
    echo " l'utilisateur n'a pas acces au dossier dir_b en ecriture"
fi
if [ -w ../dir_c/ ];then
    echo " l'utilisateur a acces au dossier dir_c en ecriture"
else
    echo " l'utilisateur n'a pas acces au dossier dir_c en ecriture"
fi

if [ -r ../dir_a/ ];then
    echo " l'utilisateur a acces au dossier dir_a en lecture"
else
    echo " l'utilisateur n'a pas acces au dossier dir_a en lecture"
fi
if [ -r ../dir_b/ ];then
    echo " l'utilisateur a acces au dossier dir_b en lecture"
else
    echo " l'utilisateur n'a pas acces au dossier dir_b en lecture"
fi
if [ -r ../dir_c/ ];then
    echo " l'utilisateur a acces au dossier dir_c en lecture"
else
    echo " l'utilisateur n'a pas acces au dossier dir_c en lecture"
fi

if [ -x ../dir_a/ ];then
    echo " l'utilisateur a acces au dossier dir_a en recherche"
else
    echo " l'utilisateur n'a pas acces au dossier dir_a en recherche"
fi
if [ -x ../dir_b/ ];then
    echo " l'utilisateur a acces au dossier dir_b en recherche"
else
    echo " l'utilisateur n'a pas acces au dossier dir_b en recherche"
fi
if [ -x ../dir_c/ ];then
    echo " l'utilisateur a acces au dossier dir_c en recherche"
else
    echo " l'utilisateur n'a pas acces au dossier dir_c en recherche"
fi

if [ -w ../dir_a/fichier1 ];then
    echo " l'utilisateur a acces au fichier fichier1 du repertoire dir_a en ecriture"
else
    echo " l'utilisateur n'a pas acces au fichier fichier1 du repertoire dir_a en ecriture"
fi
if [ -r ../dir_a/fichier1 ];then
    echo " l'utilisateur a acces au fichier fichier1 du repertoire dir_a en lecture"
else
    echo " l'utilisateur n'a pas acces au fichier fichier1 du repertoire dir_a en lecture"
fi

if [ -w ../dir_b/fichier1 ];then
    echo " l'utilisateur a acces au fichier fichier1 du repertoire dir_b en ecriture"
else
    echo " l'utilisateur n'a pas acces au fichier fichier1 du repertoire dir_b en ecriture"
fi
if [ -r ../dir_b/fichier1 ];then
    echo " l'utilisateur a acces au fichier fichier1 du repertoire dir_b en lecture"
else
    echo " l'utilisateur n'a pas acces au fichier fichier1 du repertoire dir_b en lecture"
fi