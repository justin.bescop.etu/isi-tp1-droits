#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
        FILE *f;
        int c;
        printf("uid :%d, euid :%d, gid :%d,egid :%d\n",getuid(),geteuid(),getgid(),getegid()) ;
        if(argc < 2 ) {
                printf("Missing argument\n");
                exit(EXIT_FAILURE);
        }
        printf("Hello main\n");
        f = fopen(argv[1], "r");
        if(f==NULL) {
                perror("Cannot open file");
                exit(EXIT_FAILURE);
        }
        printf("File opens correctly\n");
        while((c=fgetc(f)) != EOF){ putchar(c);}
        fclose(f);
        return 0;
}